'use strict';
 
var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	sass = require('gulp-sass'),
	gls = require('gulp-live-server');
 
gulp.task('sass', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
			browsers: ['last 15 versions'],
			cascade: false
		}))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('serve', function() {
  var server = gls.static('./', 8888);
  server.start();
  gulp.watch(['css/**/*.css', './**/*.html', 'js/**/*.js'], function (file) {
    server.notify.apply(server, [file]);
  });
});

gulp.task('default', ['sass:watch', 'serve']);