<?php
    //database configuration
    include 'db.php';
    
    //get search term
    $searchTerm = $_POST['term'];
    
    //get matched data from skills table
    $sql = "SELECT DISTINCT author, genre FROM authors WHERE author LIKE '%".$searchTerm."%' ORDER BY author ";
   $result = $conn->query($sql);

    if ($result->num_rows > 0) {
         // output data of each row
         while($row = $result->fetch_array()) {
             // $array[] = $row;
             echo "<li data-genre='" .$row["genre"]. "'><a href='#' data-genre='" .$row["genre"]. "'>". $row["author"] ."</a></li>";
             // echo json_encode($array);
         }
    } else {
         echo "<li><a>No matches</a></li>";
    }

    $conn->close();
?>