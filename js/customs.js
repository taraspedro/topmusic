var total = 0;
var arr = [];

$(function() {
	
	$('ul.authors li a').first().addClass('active');

	$('ul.authors').on("click", "a", function(e) {
		e.preventDefault();
		var author = $(this).text();
		// console.log(author);
		$.get('bin/songs.php', {"author":author},function(data){
			$('ul.songs').html(data);
		});
		$('ul.authors li a.active').removeClass('active');
		$(this).addClass('active');
	});

	// vertical slider	
	var totalHeight, limitHeight;
	function verticalHeight(selector) {
		    totalHeight = selector.height(),
			limitHeight = $('.list-wrapper').height();
	}
	verticalHeight($('ul.authors'));
	$(window).resize(function(){
		verticalHeight($('ul.authors'));
	});
	
	$('.authors-footer').on('click',function() {			
		if(totalHeight-limitHeight>0 && totalHeight/limitHeight<2){
			var top = limitHeight-totalHeight,
				topPx = {'top':top};
			$('ul.authors li').animate(topPx, 'fast');
			limitHeight + totalHeight - limitHeight;
		}	
		else if(totalHeight/limitHeight>2){
			var top = limitHeight,
				topPx = {'top':top};
			$('ul.authors li').animate(topPx);
			limitHeight=limitHeight + limitHeight;
		}
		console.log(totalHeight,limitHeight);		
	});

	$('.authors-header').on('click',function(){
		$('ul.authors li').animate({'top':'0px'});
		limitHeight = $('.list-wrapper').height();
	});

	var totalFavHeight, limitFavHeight;
	function verticalFavHeight(selector){
		totalFavHeight = selector.height();
		limitFavHeight = $('.list-wrapper').height();
	};
	verticalFavHeight($('ul.favorites'));
	$(window).resize(function(){
		verticalFavHeight($('ul.favorites'));
	});
		
	$('.favorites-footer').on('click',function() {			
		if(totalFavHeight-limitFavHeight>0 && totalFavHeight/limitFavHeight<2){
			var top = limitFavHeight-totalFavHeight,
				topPx = {'top':top};
			$('ul.favorites li').animate(topPx, 'fast');
			limitFavHeight + totalFavHeight - limitFavHeight;
		}	
		else if(totalFavHeight/limitFavHeight>2){
			var top = limitFavHeight,
				topPx = {'top':top};
			$('ul.authors li').animate(topPx, 'fast');
			limitFavHeight=limitFavHeight + limitFavHeight;
		}
		console.log(totalFavHeight,limitFavHeight);		
	});

	$('.favorites-header').on('click',function(){
		$('ul.favorites li').animate({'top':'0px'});
		limitFavHeight = $('.list-wrapper').height();
	});

	// horizontal slider
	var navItems, navVisible, dist;
	function checkWidth() {
		navItems = $('footer ul.genres li').length;
		navVisible = Math.round($('footer .slide-wrapper').width()/$('footer ul.genres li').outerWidth(true));
		dist = navItems-navVisible;
	};	
	checkWidth();
	$(window).resize(checkWidth);
	console.log(navItems,navVisible);
	$('.fa-angle-right').click(function() {		
		if(navVisible<navItems){
			$('footer ul.genres li').animate({'left':'-=90px'},'fast');
			navVisible++;
		}
		console.log(navItems,navVisible);
	});
	$('.fa-angle-left').click(function() {
		
		if(navItems-navVisible<dist){
			$('footer ul.genres li').animate({'left':'+=90px'},'fast');
			navVisible--;
		}
		console.log(navItems,navVisible);
	});	

	// filtering music by genre
	$('footer ul.genres').on("click", "a", function(e) {
		e.preventDefault();
		var genre = $(this).text();
	 	// console.log(genre);
	 	if(genre=='All'){
	 		$.get('bin/authors.php',{'genre':''},function(data){
		 		$('ul.authors').html(data);
		 		$('ul.authors li a').first().addClass('active');
		 	});
		 		$.get('bin/songs.php', {"author":''},function(data){
				$('ul.songs').html(data);
			});
	 	}
	 	else{
		 	$.get('bin/authors.php',{'genre':genre},function(data){
		 		$('ul.authors').html(data);
		 		$('ul.authors li a').first().addClass('active');
		 		var firstItem = $('ul.authors li a').first().text();
		 		$.get('bin/songs.php', {"author":firstItem},function(data){
				$('ul.songs').html(data);
			});
	 	});
	 	}
	 	$('footer ul.genres li a.active').removeClass('active');
	 	$(this).addClass('active');	 
	 	totalHeight = $('ul.authors').height();
		limitHeight = $('.list-wrapper').height();			
	});

	$('footer ul.genres li').on("mousedown", function(e) {
		$(this).css('box-shadow','none');
	});
	
	$('footer ul.genres li').on("mouseup", function(e) {
		$(this).css('box-shadow','5px 5px 5px rgba(68, 68, 68, 0.6)');
	});	

	// media player settings
	$('ul.songs').on('click', 'a', function(e){
		e.preventDefault();
		$('ul.songs .playing').removeClass('playing');
		$('ul.favorites .playing').removeClass('playing');
		$(this).addClass('playing');
		var author = $(this).data('author');
		var genre = $(this).data('genre');
		var song = $(this).text();
		var stars = $(this).data('stars')*1;
		$('.music-info').text(author + ' / ' + genre);
		$('.song-description').text(author + ' - ' + song);
		$('.media-controllers .fa-play').removeClass('fa-play').addClass('fa-pause');
		var src = $(this).attr('href');
		$('.audio-player').attr('src', src);
		$('.audio-player').trigger('load');
		$('.audio-player').trigger('play');		
		setInterval(function() {
			var aud = document.getElementById("myAudio");
			var seconds = aud.duration;
			var current = $('.audio-player').prop('currentTime');
			currentPosition = current/seconds*100;
			$('.media-toddler .progress-bar').attr('style','width:' + currentPosition + '%');
			// console.log(current);
		},1000);
		for(i=0;i<=4;i++){
			$('ul.stars li').eq(i).find('i').css('color','#fff');
		}
		for(i=0;i<=stars-1;i++){
			$('ul.stars li').eq(i).find('i').css('color','yellow');
		}
		if($('ul.songs li').length>2){
			total = $('ul.songs li').length;
			arr=[];
			while(arr.length < total){
			  var randomnumber=Math.ceil(Math.random()*total)-1;
			  var found=false;
			  for(var i=0;i<arr.length;i++){
				if(arr[i]==randomnumber){found=true;break}
			  }
			  if(!found)arr[arr.length]=randomnumber;
			}		
			console.log(total,arr);
		}			
	});

	$('ul.favorites').on('click', 'a', function(e){
		e.preventDefault();
		$('ul.favorites .playing').removeClass('playing');
		$('ul.songs .playing').removeClass('playing');
		$(this).addClass('playing');
		var author = $(this).data('author');
		var genre = $(this).data('genre');
		var song = $(this).text();
		var stars = $(this).data('stars')*1;
		$('.music-info').text(author + ' / ' + genre);
		$('.song-description').text(author + ' - ' + song);
		$('.media-controllers .fa-play').removeClass('fa-play').addClass('fa-pause');
		var src = $(this).attr('href');
		$('.audio-player').attr('src', src);
		$('.audio-player').trigger('load');
		$('.audio-player').trigger('play');		
		setInterval(function() {
			var aud = document.getElementById("myAudio");
			var seconds = aud.duration;
			var current = $('.audio-player').prop('currentTime');
			currentPosition = current/seconds*100;
			$('.media-toddler .progress-bar').attr('style','width:' + currentPosition + '%');
			// console.log(current);
		},1000);
		for(i=0;i<=4;i++){
			$('ul.stars li').eq(i).find('i').css('color','#fff');
		}
		for(i=0;i<=stars-1;i++){
			$('ul.stars li').eq(i).find('i').css('color','yellow');
		}
		if($('ul.favorites li').length>2){
			total = $('ul.favorites li').length;
			arr=[];
			while(arr.length < total){
			  var randomnumber=Math.ceil(Math.random()*total)-1;
			  var found=false;
			  for(var i=0;i<arr.length;i++){
				if(arr[i]==randomnumber){found=true;break}
			  }
			  if(!found)arr[arr.length]=randomnumber;
			}		
			console.log(total,arr);
		}
	});

	$('.media-controllers').on('click', 'i.global', function() {
		$(this).toggleClass('fa-play fa-pause');
		if($('.media-controllers i.fa-play').length == 0){
			$('.audio-player').trigger('play');
			setInterval(function() {	
				var vid = document.getElementById("myAudio");
				var seconds = vid.duration;
				var current = $('.audio-player').prop('currentTime');
				currentPosition = current/seconds*100;
				$('.media-toddler .progress-bar').attr('style','width:' + currentPosition + '%');
				// console.log(current);
			},500);
		}
		else{
			$(".audio-player").trigger('pause');
		}
	});

	$('i.fa-forward').on('click', function() {
		if($('ul.songs a.playing').length == 1){
			var current = $('ul.songs a.playing').parent().index() +1;
			var total = $('ul.songs li').length;
			// console.log(current, total);
			if(current<total){
				var src = $('ul.songs a.playing').parent().next().find('a').attr('href');
				var author = $('ul.songs a.playing').parent().next().find('a').data('author');
				var song = $('ul.songs a.playing').parent().next().find('a').text();
				$('.song-description').text(author + ' - ' + song);
				// console.log(src);
				$('.audio-player').attr('src', src);
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
				$('ul.songs a.playing').removeClass('playing');
				$('ul.songs a[href="'+src+'"]').addClass('playing');
				if($('.media-controllers i.fa-play').length == 1){
					$('.media-controllers i.fa-play').removeClass('fa-play').addClass('fa-pause');
				}
			}
		}
		else if($('ul.favorites a.playing').length == 1){
			var current = $('ul.favorites a.playing').parent().index() +1;
			var total = $('ul.favorites li').length;
			// console.log(current, total);
			if(current<total){
				var src = $('ul.favorites a.playing').parent().next().find('a').attr('href');
				var author = $('ul.favorites a.playing').parent().next().find('a').data('author');
				var song = $('ul.favorites a.playing').parent().next().find('a').text();
				$('.song-description').text(author + ' - ' + song);
				// console.log(src);
				$('.audio-player').attr('src', src);
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
				$('ul.favorites a.playing').removeClass('playing');
				$('ul.favorites a[href="'+src+'"]').addClass('playing');
				if($('.media-controllers i.fa-play').length == 1){
					$('.media-controllers i.fa-play').removeClass('fa-play').addClass('fa-pause');
				}
			}
		}		
	});

	$('i.fa-backward').on('click', function() {
		if($('ul.songs a.playing').length == 1){
			var current = $('ul.songs a.playing').parent().index();
			var total = $('ul.songs li').length;
			if(total-current<total){
				var src = $('ul.songs a.playing').parent().prev().find('a').attr('href');
				var author = $('ul.songs a.playing').parent().prev().find('a').data('author');
				var song = $('ul.songs a.playing').parent().prev().find('a').text();
				$('.song-description').text(author + ' - ' + song);
				// console.log(src);
				$('.audio-player').attr('src', src);
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
				$('ul.songs a.playing').removeClass('playing');
				$('ul.songs a[href="'+src+'"]').addClass('playing');
				if($('.media-controllers i.fa-play').length == 1){
					$('.media-controllers i.fa-play').removeClass('fa-play').addClass('fa-pause');
				}
			}
		}
		else if($('ul.favorites a.playing').length == 1){
			var current = $('ul.favorites a.playing').parent().index();
			var total = $('ul.favorites li').length;
			if(total-current<total){
				var src = $('ul.favorites a.playing').parent().prev().find('a').attr('href');
				var author = $('ul.favorites a.playing').parent().prev().find('a').data('author');
				var song = $('ul.favorites a.playing').parent().prev().find('a').text();
				$('.song-description').text(author + ' - ' + song);
				// console.log(src);
				$('.audio-player').attr('src', src);
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
				$('ul.favorites a.playing').removeClass('playing');
				$('ul.favorites a[href="'+src+'"]').addClass('playing');
				if($('.media-controllers i.fa-play').length == 1){
					$('.media-controllers i.fa-play').removeClass('fa-play').addClass('fa-pause');
				}
			}
		}		
	});
	$('.audio-player').on('ended', function() {

		if($('ul.songs li').length>1 && $('ul.favorites li a').filter('.playing').length == 0 && $('.media-options i.fa-random.active').length == 0){
			var current = $('ul.songs a.playing').parent().index();
			var total = $('ul.songs li').length;
			if(total-current>1){
				var src = $('ul.songs a.playing').parent().next().find('a').attr('href');
				var author = $('ul.songs a.playing').parent().next().find('a').data('author');
				var song = $('ul.songs a.playing').parent().next().find('a').text();
				$('.audio-player').attr('src', src);
				$('.song-description').text(author + ' - ' + song);
				$('ul.songs a.playing').removeClass('playing');
				$('ul.songs a[href="'+src+'"]').addClass('playing');
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
			}
			else{
				$('.audio-player').trigger('load');
				$('.media-controllers i.global').removeClass('fa-pause').addClass('fa-play');
			}		
		}
		else if($('ul.favorites li').length>1 && $('ul.songs li').find('.playing').length == 0 && $('.media-options i.fa-random.active').length == 0){
			var current = $('ul.favorites a.playing').parent().index();
			var total = $('ul.favorites li').length;
			if(total-current>1){
				var src = $('ul.favorites a.playing').parent().next().find('a').attr('href');
				var author = $('ul.favorites a.playing').parent().next().find('a').data('author');
				var song = $('ul.favorites a.playing').parent().next().find('a').text();
				$('.audio-player').attr('src', src);
				$('.song-description').text(author + ' - ' + song);
				$('ul.favorites a.playing').removeClass('playing');
				$('ul.favorites a[href="'+src+'"]').addClass('playing');
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
			}
			else{
				$('.audio-player').trigger('load');
				$('.media-controllers i.global').removeClass('fa-pause').addClass('fa-play');
			}
		}
		else if($('ul.songs li').length>2 && $('ul.favorites li').find('.playing').length == 0 && $('.media-options i.fa-random.active').length == 1){
			var x=arr[0],
				src = $('ul.songs li').eq(x).find('a').attr('href'),
				author = $('ul.songs li').eq(x).find('a').data('author'),
				song = $('ul.songs li').eq(x).find('a').text();
			arr = arr.slice(1);
			console.log(window.total,window.arr,x,src,author,song);	
			if(author!=undefined){
				$('.audio-player').attr('src', src);
				$('.song-description').text(author + ' - ' + song);
				$('ul.songs a.playing').removeClass('playing');
				$('ul.songs a[href="'+src+'"]').addClass('playing');
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
			}
			else{
				$('.audio-player').trigger('load');
				$('.media-controllers i.global').removeClass('fa-pause').addClass('fa-play');
				if($('ul.songs li').length>2){
					total = $('ul.songs li').length;
					arr=[];
					while(arr.length < total){
					  var randomnumber=Math.ceil(Math.random()*total)-1;
					  var found=false;
					  for(var i=0;i<arr.length;i++){
						if(arr[i]==randomnumber){found=true;break}
					  }
					  if(!found)arr[arr.length]=randomnumber;
					}		
					console.log(total,arr);
				}
			}					
		}
		else if($('ul.favorites li').length>2 && $('ul.songs li').find('.playing').length == 0 && $('.media-options i.fa-random.active').length == 1){
			var x=arr[0],
				src = $('ul.favorites li').eq(x).find('a').attr('href'),
				author = $('ul.favorites li').eq(x).find('a').data('author'),
				song = $('ul.favorites li').eq(x).find('a').text();
			arr = arr.slice(1);
			console.log(window.total,window.arr,x,src,author,song);	
			if(author!=undefined){
				$('.audio-player').attr('src', src);
				$('.song-description').text(author + ' - ' + song);
				$('ul.favorites a.playing').removeClass('playing');
				$('ul.favorites a[href="'+src+'"]').addClass('playing');
				$('.audio-player').trigger('load');
				$('.audio-player').trigger('play');
			}
			else{
				$('.audio-player').trigger('load');
				$('.media-controllers i.global').removeClass('fa-pause').addClass('fa-play');
				if($('ul.favorites li').length>2){
					total = $('ul.favorites li').length;
					arr=[];
					while(arr.length < total){
					  var randomnumber=Math.ceil(Math.random()*total)-1;
					  var found=false;
					  for(var i=0;i<arr.length;i++){
						if(arr[i]==randomnumber){found=true;break}
					  }
					  if(!found)arr[arr.length]=randomnumber;
					}		
					console.log(total,arr);
				}
			}
		}
		
		else{
			$('.audio-player').trigger('load');
			$('.media-controllers i.global').removeClass('fa-pause').addClass('fa-play');
		}
	});		

	$('.media-options').on('click', 'i.fa-volume-off', function() {
		$(this).toggleClass('active');
		var muted = $('.audio-player').prop('muted');
		$('.audio-player').prop('muted', !muted);
	});

	$('.media-options').on('click', 'i.fa-repeat', function() {
		$(this).toggleClass('active');
		var repeated = $('.audio-player').prop('loop');
		$('.audio-player').prop('loop', !repeated);
	});

	$('.media-options').on('click', 'i.fa-random', function() {
		$(this).toggleClass('active');
	});

	$('.media-toddler .progress').on('click', function(e) {
		var offset = $(this).offset();
		var setPosition = e.pageX - offset.left;
		var trackLength = $(this).width();
		var dur = $('.audio-player').prop('duration');
		var trackValue = setPosition/trackLength*dur;
		console.log(trackValue);
		$(".audio-player").prop("currentTime",trackValue);
		$('.media-toddler .progress-bar').attr('style','width:' + setPosition/trackLength*100 + '%');
	});	

	$('.media-options .progress').on('click', function(e) {
		var offset = $(this).offset();
		var setPosition = e.pageX - offset.left;
		var volumeLength = $(this).width();
		// console.log(setPosition);
		var volumeValue = setPosition/volumeLength;
		$(".audio-player").prop("volume",volumeValue);
		$('.media-options .progress-bar').attr('style','width:' + volumeValue*100 + '%');
		var durak = $('.audio-player').prop('duration');
		// console.log(durak);
	});

    // footer menu toggle
    $('.footer-toggle a').on('click', function(e){
    	e.preventDefault();
    	$('ul.genres').slideToggle('fast');
    });
    function showNav(){
    	if($(window).width()>767){
    		$('ul.genres:hidden').show();
    	}
    	else{
    		$('ul.genres').hide();
    	}
    }
    $(window).resize(showNav);

    // stars    
    $('ul.stars li.star i').on('mouseover', function() {
    	var starNumber = $(this).parent().index();
    	var i;
    	if($(this).css('color') == 'rgb(255, 255, 255)'){
    		for (i = 0; i <= starNumber; i++) {
			    $('ul.stars li.star').eq(i).find('i').css('color','yellow');
			}
    	}
    	else{
    		for (i = 0; i <= $('ul.stars li.star').length - 1; i++) {
			    $('ul.stars li.star').eq(i).find('i').css('color','#fff');
			}
			for (i = 0; i <= starNumber; i++) {
			    $('ul.stars li.star').eq(i).find('i').css('color','yellow');
			}
    	}    	
    });

    $('ul.stars li.star i').on('click', function() {
    	if($('ul.songs a.playing').length == 1){
    		var starNumber = $(this).parent().index() + 1,
    			starSong = $('ul.songs a.playing').text(),
    			starAuthor = $('ul.songs a.playing').data('author');
    		// console.log(starNumber,starSong,starAuthor);	
    		$.get('bin/stars.php',{"amount":starNumber,"song":starSong,"author":starAuthor},function(data){
    			for(i = 0;i <= data-1; i++){
    				$('ul.stars li.star').eq(i).find('i').css('color','yellow');
    			}
    			$('ul.songs a.playing').data('stars',data);
    		});
    		if(starNumber == 5){
    			setTimeout(function(){
    				$.get('bin/favorites.php',function(data){
		    			$('ul.favorites').html(data);
		    		});
    			}, 2000);  					
    		}    		
    	}
    	else{
    		alert('Click song to play before estimation!');
    	}
    });

    $('ul.stars li.refresh i').on('click', function() {
    	for(i=0;i<=$('ul.stars li.star').length - 1;i++){
    		$('ul.stars li.star').eq(i).find('i').css('color','#fff');
    	}
    	var starNumber = 0;
    	if($('ul.songs a.playing').length == 1){
			var starSong = $('ul.songs a.playing').text(),
				starAuthor = $('ul.songs a.playing').data('author');
    	}
    	else if($('ul.favorites a.playing').length == 1){
    		var starSong = $('ul.favorites a.playing').text(),
				starAuthor = $('ul.favorites a.playing').data('author');
    	}
    	
		$.get('bin/stars.php',{"amount":starNumber,"song":starSong,"author":starAuthor});
		setTimeout(function(){
			$.get('bin/favorites.php',function(data){
    			$('ul.favorites').html(data);
    		});
		}, 2000);
    });
 
    //search settings
    function find (){
    	if($(window).width()<974){
    		var uncle = $('.player-image img').width();
    		$('.find').width(uncle).css('margin','0 auto 10px');
    	}
    	else{
    		$('.find').css({'width':'auto', 'margin':'auto'});
    	}
    };

    find();
    $(window).resize(find);

    $('input.music-search').on('keyup',function(){
    	var term = $(this).val();
    	console.log(term);
    	$.post('bin/autocomplete.php',{'term':term},function(data){
    		$('ul.authors').html(data);
    	});
    });

    // share music info
    $('ul.socials li.fb a').on('click',function(e){
    	var postSong = $('.song-description').text();
    	e.preventDefault();
    	FB.ui({
		  method: 'feed',
		  name: 'I love this song!',
		  link: 'http://topmusic.ho.ua/',
		  caption: 'Check up the best music on this site!',
		  description: postSong,
		}, function(response){});
    });

    $('.popup').click(function(event) {
    		var postSong = $('.song-description').text();
    		postSong = postSong.split(" ").join("%20");
    		var currentHref = $(this).attr('href');
    		$(this).attr('href',currentHref+postSong);
    		// console.log(postSong);
		    var width  = 575,
	        height = 400,
	        left   = ($(window).width()  - width)  / 2,
	        top    = ($(window).height() - height) / 2,
	        url    = this.href,
	        opts   = 'status=1' +
	                 ',width='  + width  +
	                 ',height=' + height +
	                 ',top='    + top    +
	                 ',left='   + left;
	    
	    window.open(url, 'twitter', opts);
	 
	    return false;
	});

	Share = {
		vkontakte: function(purl, ptitle, pimg, text) {
			url  = 'http://vkontakte.ru/share.php?';
			url += 'url='          + encodeURIComponent(purl);
			url += '&title='       + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&image='       + encodeURIComponent(pimg);
			url += '&noparse=true';
			Share.popup(url);
		},

		popup: function(url) {
			window.open(url,'','toolbar=0,status=0,width=626,height=436');
		}
	};

	$('ul.socials li.vk a').on('click', function(e){
		e.preventDefault();
		var postSong = $('.song-description').text();
	    Share.vkontakte('http://topmusic.ho.ua/','I love this song!','images/foto.jpg',postSong);
	});    
});