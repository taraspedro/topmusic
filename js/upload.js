$(document).ready(function() {
  window.reader = new ID3Reader($("#the_file")[0]);
  reader.onload = function(tag) {
    console.log(tag.artist, tag.title, tag.album, tag.genre);
    if(!tag) {
      console.log("Unable to read ID3 Tag of file");
      return false;
    }
    $("#song-upload input[name='artist']").val(tag.artist);
    $("#song-upload input[name='song']").val(tag.title);		    
  };

  $('form#song-upload').submit(function() {
  	var formData = new FormData($(this)[0]);
  	$(this).append('<i class="fa fa-spinner fa-pulse"></i>');
  	$.ajax({
        url: 'bin/upload.php',
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            $(this).find('i.fa.fa-spinner.fa-pulse').remove();
            alert(data);
        },
        cache: false,
        contentType: false,
        processData: false
    	});
	});
});