<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TopMusic</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- 	<form id="song-upload" method="post" enctype="multipart/form-data">
		<input type="file" name="file" id="the_file" />
		<input type="text" name="artist" placeholder="Artist" required>
		<input type="text" name="song" placeholder="Song name" required>
		<input type="text" name="genre" placeholder="Genre" required>
		<button type="submit">Submit</button>
	</form> -->
	<div class="container">
		<h2 class="white-color">Choose a song you want to upload!</h2>
		<p>(Only mp3 is supported)</p>
			<form id="song-upload" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label class="white-color" for="inputLogin">File</label>
					<input type="file" name="file" id="the_file" />
				</div>
				<div class="form-group">
					<label class="white-color">Artist</label>
					<input class="form-control" type="text" name="artist" placeholder="Artist" required>
				</div>
				<div class="form-group">
					<label class="white-color" for="inputPassword">Song</label>
					<input class="form-control" type="text" name="song" placeholder="Song name" required>
				</div>
				<div class="form-group">
					<label class="white-color" for="inputPassword2">Choose a song genre</label>
					<select class="form-control" name="genre" required>
						<option disabled selected> -- select an option -- </option>
						<option value="New Metal">New Metal</option>
						<option value="Rock">Rock</option>
						<option value="Blues">Blues</option>
						<option value="Grunge">Grunge</option>
						<option value="Indie">Indie</option>
						<option value="Alternative">Alternative</option>
						<option value="Hard Rock">Hard Rock</option>
						<option value="Funk">Funk</option>
						<option value="Metal">Metal</option>
						<option value="New Wave">New Wave</option>
						<option value="Pop">Pop</option>
					</select>
				</div>
				<button class="btn btn-primary" type="submit"><i class="fa fa-share-square-o"></i>
Submit</button>&nbsp;or&nbsp;
        		<a href="index.php" type="button" class="btn btn-success"><i class="fa fa-arrow-circle-left"></i>
Back to the main page</a>
			</form>
	</div>
	


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>	
	<script src="js/ID3Reader.js"></script>
	<script src="js/upload.js"></script>

</body>
</html>