-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Хост: db2.ho.ua
-- Час створення: Лют 15 2016 р., 21:35
-- Версія сервера: 5.6.27-log
-- Версія PHP: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `topmusic`
--

-- --------------------------------------------------------

--
-- Структура таблиці `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `author` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `src` varchar(100) NOT NULL,
  `stars` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `authors`
--

INSERT INTO `authors` (`id`, `author`, `genre`, `name`, `src`, `stars`) VALUES
(42, 'Koßn', 'New Metal', 'Freak on a Leash', 'audio/02 - Freak on a Leash.mp3', 0),
(43, 'Koßn', 'New Metal', 'Dead Bodies Everywhere', 'audio/04 - Dead Bodies Everywhere.mp3', 0),
(44, 'Limp Bizkit', 'New Metal', 'My Generation', 'audio/03 - My Generation.mp3', 0),
(45, 'Limp Bizkit', 'New Metal', 'Take A Look Around', 'audio/10 - Take A Look Around.mp3', 0),
(46, 'Bon Jovi', 'Rock', 'Lie To Me', 'audio/05. Lie To Me.mp3', 0),
(47, 'Gary Clark Jr.', 'Blues', 'When My Train Pulls In', 'audio/04 -  When My Train Pulls In.mp3', 0),
(48, 'Foo Fighters', 'Grunge', 'Times Like These', 'audio/Foo Fighters - Time like these.mp3', 0),
(49, 'Foo Fighters', 'Grunge', 'Walk', 'audio/Foo Fighters - Walk.mp3', 0),
(50, 'Foo Fighters', 'Grunge', 'Best of You', 'audio/Foo Fighters - Best Of You.mp3', 0),
(51, 'John Mayer', 'Blues', 'Bigger Than My Body', 'audio/02 John Mayer Heavier Things Bigger Than My Body.mp3', 0),
(52, 'John Mayer', 'Blues', 'Come Back to Bed', 'audio/05 John Mayer Heavier Things Come Back to Bed.mp3', 0),
(53, 'John Mayer', 'Blues', 'Wheel', 'audio/10 John Mayer Heavier Things Wheel Pop.mp3', 0),
(54, 'Kaiser Chiefs', 'Indie', 'Ruby', 'audio/01. Ruby.mp3', 0),
(55, 'Kaiser Chiefs', 'Indie', 'The Angry Mob', 'audio/02. The Angry Mob.mp3', 0),
(56, 'Karnivool', 'Alternative', 'All I Know', 'audio/06. All I Know.mp3', 0),
(57, 'Karnivool', 'Alternative', 'Umbra', 'audio/05. Umbra.mp3', 0),
(58, 'Metallica', 'Hard Rock', 'No Leaf Clover', 'audio/08 - No Leaf Clover.mp3', 0),
(59, 'Metallica', 'Hard Rock', 'Outlaw Torn', 'audio/06 - Outlaw Torn.mp3', 0),
(60, 'Nirvana', 'Grunge', 'Sappy', 'audio/17-Sappy.mp3', 0),
(61, 'Nirvana', 'Grunge', 'All Apologies', 'audio/12-All Apologies.mp3', 0),
(62, 'Океан Ельзи', 'Rock', 'Коли тебе нема', 'audio/12. Коли тебе нема.mp3', 0),
(63, 'Океан Ельзи', 'Rock', 'Там де нас нема', 'audio/20. Там, де нас нема.mp3', 0),
(64, 'Океан Ельзи', 'Rock', 'Вставай', 'audio/05. Вставай.mp3', 0),
(65, 'RATM', 'Rapcore', 'Sleep Now In The Fire', 'audio/05 -  Sleep Now In The Fire.mp3', 0),
(66, 'RATM', 'Rapcore', 'Killing in the Name', 'audio/02 -  Killing in the Name.mp3', 0),
(67, 'RATM', 'Rapcore', 'Take the Power Back', 'audio/03 -  Take the Power Back.mp3', 0),
(69, 'Red Hot Chili Peppers', 'Funk-metal', 'Scar Tissue', 'audio/A3 - Scar Tissue.mp3', 0),
(70, 'Red Hot Chili Peppers', 'Funk-metal', 'Parallel Universe', 'audio/A2 - Parallel Universe.mp3', 0),
(71, 'Sting', 'New Wave', 'La belle dame sans regrets', 'audio/Sting - La belle.mp3', 0),
(72, 'SCORPIONS', 'Hard Rock', 'OBSESSION', 'audio/OBSESSION.mp3', 0),
(73, 'SCORPIONS', 'Hard Rock', 'UNDER THE SAME SUN', 'audio/UNDER THE SAME SUN.mp3', 0),
(74, 'SCORPIONS', 'Hard Rock', 'WHEN YOU CAME INTO MY LIFE', 'audio/WHEN YOU CAME INTO MY LIFE.mp3', 0),
(75, 'Steel Dragon', 'Hard Rock', 'We All Die Young', 'audio/Steel Dragon - We All Die Young (OST Rock Star).mp3', 0),
(76, 'Pink', 'Pop', 'Try', 'audio/03. Try.mp3', 0),
(77, 'Pink', 'Pop', 'Just Give Me A Reason (Feat. N', 'audio/04. Just Give Me A Reason (Feat. Nate Ruess).mp3', 0),
(78, 'Coldplay', 'Alternative', 'Lost!', 'audio/03.Lost.mp3', 0),
(79, 'Coldplay', 'Alternative', 'Life In Technicolor', 'audio/01.Life In Technicolor.mp3', 0),
(80, 'Coldplay', 'Alternative', 'Violet Hill', 'audio/08.Violet Hill.mp3', 0),
(81, 'HIM', 'Rock', 'The Funeral Of Hearts', 'audio/09. The Funeral Of Hearts.mp3', 0),
(82, 'HIM', 'Rock', 'Join Me In Death', 'audio/12. Join Me In Death.mp3', 0),
(83, 'Плач Єремії', 'Rock', 'Відшукування причетного', 'audio/04 Відшукування причетного.mp3', 0),
(84, 'Плач Єремії', 'Rock', 'Коридор', 'audio/09 Коридор.mp3', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(2, 'taras', 'taras', 'taras'),
(3, 'taras', 'taras', '1234'),
(4, 'taras', 'taras', 'taras'),
(5, 'name', 'name', 'name');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
